# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require pypi \
    py-pep517 [ backend=setuptools entrypoints=[ displaycal{,-3dlut-maker,-apply-profiles,-curve-viewer,-eecolor-to-madvr-converter,-profile-info,-scripting-client,-synthprofile,-testchart-editor,-vrml-to-x3d-converter,-vrml-to-x3d-converter-console} ] ] \
    freedesktop-desktop gtk-icon-cache

SUMMARY="Open Source Display Calibration and Characterization"
DESCRIPTION="
Calibrates and characterizes display devices using a hardware sensor,
driven by the open source color management system Argyll CMS.
Supports multi-display setups and a variety of available settings like
customizable whitepoint, luminance, black level, tone response curve
as well as the creation of matrix and look-up-table ICC profiles with
optional gamut mapping. Calibrations and profiles can be verified
through measurements, and profiles can be installed to make them
available to color management aware applications.
Profile installation can utilize Argyll CMS, Oyranos and/or GNOME
Color Manager if available, for flexible integration.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-python/numpy[python_abis:*(-)?]
        dev-python/Send2Trash[python_abis:*(-)?]
        dev-python/wxPython:=[>=2.8.11][python_abis:*(-)?]
    run:
        dev-python/distro[python_abis:*(-)?]
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/psutil[python_abis:*(-)?]
        dev-python/PyChromecast[python_abis:*(-)?] [[ description = [ Required for Chromecast support ] ]]
        measurement/argyllcms
        media-libs/SDL_mixer:2
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.9.11-fix-install.patch
)

install_one_multibuild() {
    py-pep517_install_one_multibuild

    insinto /etc/xdg/autostart
    doins misc/z-displaycal-apply-profiles.desktop

    edo rm -r "${IMAGE}"/$(python_get_sitedir)/var

    edo rm -rf "${IMAGE}"/usr/share
    edo mv "${IMAGE}"/usr/{$(exhost --target),}/share
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

