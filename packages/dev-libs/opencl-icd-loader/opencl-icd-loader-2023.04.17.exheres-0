# Copyright 2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=OpenCL-ICD-Loader tag=v${PV} ] \
    cmake

SUMMARY="Khronos official OpenCL ICD Loader"
DESCRIPTION="
OpenCL defines an Installable Client Driver (ICD) mechanism to allow developers to build
applications against an Installable Client Driver loader (ICD loader) rather than linking their
applications against a specific OpenCL implementation. The ICD Loader is responsible for:
* Exporting OpenCL API entry points
* Enumerating OpenCL implementations
* Forwarding OpenCL API calls to the correct implementation
"
HOMEPAGE+=" https://www.khronos.org/opencl/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/opencl-headers[>=2023.04.17]
        virtual/pkg-config
    build+run:
        !dev-libs/ocl-icd [[
            description = [ dev-libs/opencl-icd-loader replaces dev-libs/ocl-icd ]
            resolution = uninstall-blocked-after
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_DATADIR:PATH=/usr/$(exhost --target)/lib
    -DENABLE_OPENCL_LAYERINFO:BOOL=TRUE
    -DENABLE_OPENCL_LAYERS:BOOL=TRUE
    -DOPENCL_ICD_LOADER_BUILD_SHARED_LIBS:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    '-DOPENCL_ICD_LOADER_BUILD_TESTING:BOOL=TRUE -DOPENCL_ICD_LOADER_BUILD_TESTING:BOOL=FALSE'
)

