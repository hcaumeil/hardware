# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Copyright 2023 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ slotted=true ]
require alternatives

export_exlib_phases src_prepare src_install

SUMMARY="libclc is an implementation of the library requirements of the OpenCL C programming language"

# There doesn't seem to be any test ninja target
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/clang:${LLVM_SLOT}
    build+run:
        dev-util/spirv-llvm-translator:${SLOT}
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_PREFIX:STRING="${LLVM_PREFIX}"
    -DCMAKE_INSTALL_DATADIR="${LLVM_PREFIX}"/share
    -DLLVM_DIR:PATH="${LLVM_PREFIX}"/lib/cmake/llvm
)

libclc_src_prepare() {
    default

    edo sed -i "${CMAKE_SOURCE}"/libclc.pc.in -e '/^libexecdir/s:@CMAKE_INSTALL_PREFIX@/::'
}

libclc_src_install() {
    cmake_src_install

    other_alternatives=(
        /usr/$(exhost --target)/lib/pkgconfig/libclc.pc /usr/$(exhost --target)/lib/llvm/${SLOT}/share/pkgconfig/libclc.pc
        /usr/share/clc /usr/$(exhost --target)/lib/llvm/${SLOT}/share/clc
    )
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

