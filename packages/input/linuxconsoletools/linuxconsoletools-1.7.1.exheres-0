# Copyright 2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=${PN//tools} ]

SUMMARY="Tools for connecting joysticks & legacy devices to the kernel's input subsystem"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="systemd"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/SDL:2
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr/$(exhost --target)
)

src_prepare() {
    # Reported, https://sourceforge.net/p/linuxconsole/feature-requests/6
    edo sed \
        -e 's:shell pkg-config:shell $(PKG_CONFIG):g' \
        -i utils/Makefile

    edo sed \
        -e 's:$(DESTDIR)/lib:$(DESTDIR)/$(PREFIX)/lib:g' \
        -i utils/Makefile

    edo sed \
        -e 's:$(PREFIX)/share:/usr/share:g' \
        -i {docs,utils}/Makefile

    default
}

src_compile() {
    emake \
        $(option systemd SYSTEMD_SUPPORT=1 SYSTEMD_SUPPORT=0)
}

