# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=medusalix tag=v${PV} ]

SUMMARY="Driver for Xbox One and Xbox Series X|S accessories"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        app-arch/cabextract
        net-misc/curl
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/bbf0dcc484c3f5611f4e375da43e0e0ef08f3d18.patch
    "${FILES}"/eaa55d0383839eb805f1bf2b75766311956de6e6.patch
    "${FILES}"/1b4344ab8d9d7891f843dc993b0c5447f46c9889.patch
)

src_prepare() {
    default

    edo sed \
        -e "s:#VERSION#:${PV}:g" \
        -i dkms.conf
}

src_install() {
    insinto /usr/src/${PNV}
    doins {Kbuild,dkms.conf}
    doins -r {bus,driver,transport}

    insinto /etc/modprobe.d
    newins install/modprobe.conf ${PN}-blacklist.conf

    newbin install/firmware.sh xone-get-firmware.sh

    emagicdocs
}

pkg_postinst() {
    elog "Run xone-get-firmware.sh to download and extract the firmware"
    elog "required for the Xbox Wireless Adapter USB dongle."
}

