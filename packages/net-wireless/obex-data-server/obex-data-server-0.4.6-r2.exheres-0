# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="D-Bus service providing high-level OBEX client and server side functionality"
DESCRIPTION="
obex-data-server is D-Bus service providing high-level OBEX client and server side functionality
(currently supports OPP, FTP, BIP profiles, Bluetooth and USB transports). It was originally
developed as Google Summer of Code 2007 project. obex-data-server is maintained by Tadas Dailyda
(tadas_at_dailyda_dot_com, tadas in #ods and #bluez channels at Freenode). obex-data-server depends
on openobex, bluez, glib, dbus and dbus-glib.
"
HOMEPAGE="http://wiki.muiline.com/${PN}"
DOWNLOADS="http://tadas.dailyda.com/software/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gdk-pixbuf [[ description = [ Complete Bluetooth BIP support using gdk-pixbuf ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/dbus-glib:1[>=0.70]
        dev-libs/glib:2[>=2.10]
        dev-libs/libusb:0.1 [[ note = [ Configure switch doesn't fully disable USB ] ]]
        net-wireless/bluez[>=3.34]
        net-wireless/openobex[>=1.7.1]
        gdk-pixbuf? ( x11-libs/gdk-pixbuf:2.0 )
"
DEFAULT_SRC_PREPARE_PATCHES=( -p0 "${FILES}"/obex-data-server-openobex_EnumerateInterfaces.patch )

src_configure() {
    local image_backend=no

    option gdk-pixbuf && image_backend=gdk-pixbuf

    econf --enable-bip=${image_backend}
}

