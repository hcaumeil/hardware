# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=mobian1 subgroup=devices new_download_scheme=true ]
require meson

export_exlib_phases src_prepare

SUMMARY="Manager daemon for the Quectel EG25 mobile broadband modem"
DESCRIPTION="
eg25-manager is a daemon for managing the Quectel EG25 modem found on the
Pine64 PinePhone.
It implements the following features:

cleanly power on/off the modem
configure/check essential parameters (such as the audio format) on startup
monitor the modem state through ModemManager
put the modem in low-power mode when suspending the system, and restore it
back to normal behavior when resuming
monitor the modem state on resume and recover it if needed
"
HOMEPAGE="https://gitlab.com/mobian1/devices/eg25-manager"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/scdoc
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        dev-libs/libgpiod
        dev-libs/libusb:1
        gnome-desktop/libgudev
        net-misc/curl
        net-wireless/ModemManager
"

eg25-manager_src_prepare() {
    meson_src_prepare

    edo sed -e "s:lib/:$(exhost --target)/lib/:" -i meson.build
}

